![Kalyptio Logo](./kalyptio.png "Kalyptio Logo")
# Kalyptio Fullstack Code Challenge

A garage parking which manages plenty of car spaces needs to develop an app in order to create, delete, list and filter their available parking spaces. Your mission is to build a webapp in order to solve the company's needs for the following use case:

A person is searching for a parking space between the 1st of October and the 30th of April. He/she is prompted with three different options; parking space A in Street A #35, California, CA, parking space B in Street B #27, California, CA, and parking space C in Street C #7, California, CA.
Parking space A will cost 100USD per month, Parking space B will cost 175USD per month, and Parking space C will cost 200USD per month.

The platform will show him/her the spaces available with the following requirements:


---
## Server requirements description

We’d expect the server to expose the following endpoints:

1. Create new car parking.
2. List parkings (You can filter parkings by using query params).
3. List parking details.
4. Delete parkings.

Required data to create a Parking: 

1. Address.
2. Amenities.
3. Score.
4. Price.
5. Type (Public / Private).
6. Images (up to 5 images).
7. Description.

Server will be able to filter parkings according to the following params:

1. Max total price for stay.
2. Min total price for stay.
3. Type.
4. Amenities.

Backend solution should be written in NodeJs + express and shouldn´t use any external database. Database must be simulated with an array.

The solution should be written using Typescript and bonus points will be awarded for the use of Docker.

---
## Client requirements description

1. Client solution should be written in Angular +12, Vue +3 or React +17.
2. Web app must consume server app endpoints (Create, list, filter, parking detail page and delete parkings).
3. Web app must be responsive!
4. You can use any UI library like Bootstrap or Material.

We’d expect the following design when listing parkings in desktop (Individual Parking Card): 
![Kalyptio Logo](./parkcardweb.png "Parking card")


We’d expect the following design when listing parkings in mobile (Individual Parking Card): 
![Kalyptio Logo](./parkcardmobile.png "Parking card")

Parking Card must show the following info:

1. Address.
2. Amenities.
3. Score.
4. Price.
5. Image Carrousel (You can use any images. Up to 5 per card).

---
## General requirements description

The solution should:

- Be sent via email in zip format.

- Build and execute in a Unix operating system.

- Focus on solving the business problem (less boilerplate!)

- Have a clear structure.

- Be written using TypeScript.

- Be written in Angular, React or Vue for the frontend, and NodeJS + Express for the backend

- Be easy to grow with new functionality. It has to be scalable.

- Don’t include binaries, and use a dependency management tool (npm or yarn).

- Add file README.md with instructions to run the project

- Use Commit messages (include .git in zip)

---
## Evaluation
We´re going to evaluate best practices, standards, logic, clean code, maintainable, readable etc. 



### Enjoy coding!